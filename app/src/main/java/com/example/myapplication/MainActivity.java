package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

import com.elo.paymentsdk.AuthSyncService;
import com.elo.paymentsdk.Callback.BasicConfigCallback;
import com.elo.paymentsdk.SDKConstants;
import com.elo.paymentsdk.TerminalConfig;
import com.elo.paymentsdk.TransactionAPIImplementation;
import com.elo.paymentsdk.Util.LogUtil;


import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init() {
        Button btn = findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPermissions();

            }
        });


/*
        new TransactionAPIImplementation(this).beginPaymentTransaction("200.00", "abcdef", new TransactionCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                try {
                    LogUtil.showVerboseLog("amount",jsonObject.optString("amount"));
                    LogUtil.showVerboseLog("authorizationcode",jsonObject.optString("authorizationcode"));
                    LogUtil.showVerboseLog("referenceId",jsonObject.optString("referenceId"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDeclined(String s) {

            }

            @Override
            public void onReadError(String s) {

            }

            @Override
            public void onSecureTimeout(String s) {

            }

            @Override
            public void onError(String inErrorMessage) {

            }

            @Override
            public void clientAuthenticationFailed(String inErrorMessage) {

            }

            @Override
            public void networkNotAvailable() {

            }
        });
*/
    }

    public void getPermissions() {
        if (PermissionUtils.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1)) {
            if (PermissionUtils.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE, 1)) {
                try {

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(intent, 0);

                    //uploading basic configuration
                    final JSONObject jsonObject = new JSONObject();
                    jsonObject.put(SDKConstants.BARCODESCANNER_TIMER, "45");
                    jsonObject.put(SDKConstants.TERMINALPORTS_TIMER, "45");
                    jsonObject.put(SDKConstants.CONNECT_TIMEOUT, "45");
                    jsonObject.put(SDKConstants.SERVER_URL, "https://abym.in");
                    jsonObject.put(SDKConstants.SERVER_PORTS, "45");
                    jsonObject.put(SDKConstants.MERCHANT_ID, "2");
                    jsonObject.put(SDKConstants.TERMINAL_ID, "1");
                    TransactionAPIImplementation t = new TransactionAPIImplementation(this);
                    t.uploadBasicConfigFile(jsonObject, new BasicConfigCallback() {
                        @Override
                        public void onUploadSuccess() {

                        }

                        @Override
                        public void onUploadError(String s) {

                        }
                    });

                    //uploading terminal configuration.
                 /*   try {
                        String newconfigFile = Environment.getExternalStorageDirectory().toString() + "/config/emv_config_new.xml";
                        LogUtil.showDebugLog("newconfigfile", newconfigFile);
                        String filename = newconfigFile.substring(newconfigFile.lastIndexOf("/") + 1);
                        // InputStream istream = mCtx.openFileInput(configFile);
                        InputStream is = new FileInputStream(newconfigFile);
                        t.uploadTerminalConfigFile(filename, is);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void uploadTerminalConfig(String path) {
        try {
            String filename = path.substring(path.lastIndexOf("/") + 1);
            InputStream is = new FileInputStream(path);
            new TransactionAPIImplementation(this).uploadTerminalConfigFile(filename, is);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:
                getPermissions();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                Uri uri = intent.getData();
                String type = intent.getType();
                LogUtil.showDebugLog("content_file", uri.toString());
                if (uri != null) {
                    try {
                        LogUtil.showDebugLog("config_file_path", PathUtils.getPath(this, uri));
                        uploadTerminalConfig(PathUtils.getPath(this, uri));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else LogUtil.showErrorLog("error", "Back from pick with cancel status");
        }
    }
}
